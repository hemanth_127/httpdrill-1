var http = require('http')
var fs = require('fs')
var uuid = require('uuid')

var server = http.createServer(handlerequest)

function handlerequest (req, res) {
  if (req.method == 'GET' && req.url == '/') {

    fs.createReadStream('./index.html').pipe(res)

  } else if (req.method == 'GET' && req.url == '/about') {

    fs.createReadStream('./index.html').pipe(res)

  } else if (req.method == 'GET' && req.url == '/json') {

    fs.createReadStream('./index.json').pipe(res)

  } else if (req.method === 'GET' && req.url === '/uuid') {

    res.writeHead(200, { 'Content-Type': 'application/json' })

    res.end(JSON.stringify({ uuid: uuid.v4() }))

  } else if (req.method === 'GET' && req.url.startsWith('/status/')) {

    const statusCode = parseInt(req.url.split('/')[2])

    const statusObject = {

      status: statusCode,

      message: http.STATUS_CODES[statusCode]

    }

    res.end(JSON.stringify(statusObject))

  } else if (req.method === 'GET' && req.url.startsWith('/delay/')) {

    const delayInSeconds = parseInt(req.url.substring(7))

    if (!isNaN(delayInSeconds)) {

      setTimeout(() => {

        res.writeHead(200, { 'Content-Type': 'text/plain' })

        res.end('Delayed response after ' + delayInSeconds + ' seconds')

      }, delayInSeconds * 1000)
    }
  }
}

port = 8000
server.listen(port,() => {

  console.log(`Server running at ${`http://localhost:${port}`}`);
})
