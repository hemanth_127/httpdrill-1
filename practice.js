// Async await
// async function getData (params) {
//   return 'namaste'
// }
//  getData()//normal way to access the async
// .then(data => {
//   console.log(data)
// })

// async with await =-> used to handle promises

// const p1 = new Promise((resolve, reject) => {
//   //promise
//   setTimeout(() => {
//     resolve('Promise 1 resolved value')
//   }, 2000)
// })

// const p2 = new Promise((resolve, reject) => {
//   //promise
//   setTimeout(() => {
//     resolve('Promise 2 resolved value')
//   }, 5000)
// })

// await can only be used inside the async function

/*most importantly it suspend the function 
 again it comes for it and starts where it left. 
*/

// async function handlePromises () {
//   console.log('monkey D luffy')
//   // Js Engine was waiting for promise to be resolved
//   // const val2 = await p2
//   // console.log(val2)
//   const val1 = await p1

//   console.log('orey wa naru kaizuku')
//   console.log(val1)
// }

// handlePromises()

/*
 * normal way to fetch data from url
 */

// const getapi = 'https://jsonplaceholder.typicode.com/todos'
// // const api = fetch(getapi)
// // api
// //   .then(x => {
// //     return x.json()
// //   })
// //   .then(k => console.log(k))
// //   .catch(() => console.log('error'))

// // using async fetch data

// async function fetcher (api) {
//   const data = await fetch(api)
//   const jsonData = await data.json()

//   console.log(jsonData)
// }
// fetcher(getapi).catch(()=>console.log("error"))

// -------------------------------------------------
// this keyword
// it is window when it is on the browser ,
// otherwise it logs the object{}

// "user strict";
// console.log(this)

// function x() {
//   console.log(this)
// }
// x()
